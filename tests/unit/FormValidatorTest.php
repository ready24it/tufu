<?php
/**
 * Project      tufu
 * @author      Giacomo Barbalinardo <info@ready24it.eu>
 * @copyright   2017
 */

namespace Tufu\Test;


use Symfony\Component\HttpFoundation\Request;
use Tufu\Core\FormValidator;


class FormValidatorTest extends \PHPUnit_Framework_TestCase
{
    public function testValidate()
    {
        $validator = new FormValidator();
        $request = new Request();
        $request->setMethod('POST');
        $request->request->add(array(
            'name'     => 'foo',
            'lastname' => 'bar'
        ));

        $validator->validate($request->request->all(), array(
            'name'     => 'NotBlank',
            'lastname' => 'NotBlank'
        ));

        $this->assertTrue($validator->isValid());
    }

    public function testValidateFalse()
    {
        $validator = new FormValidator();
        $request = new Request();
        $request->setMethod('POST');
        $request->request->add(array(
            'name'     => '',
            'lastname' => 'bar'
        ));

        $validator->validate($request->request->all(), array(
            'name'     => 'NotBlank',
            'lastname' => 'NotBlank'
        ));

        $this->assertFalse($validator->isValid());
    }

    public function testGetErrors()
    {
        $expected = array(
            'name' => 'NotBlank',
            'tel'  => 'NotBlank'
        );

        $validator = new FormValidator();
        $request = new Request();
        $request->setMethod('POST');
        $request->request->add(array(
            'name' => '',
            'tel'  => ''
        ));

        $validator->validate($request->request->all(), array(
            'name' => 'NotBlank',
            'tel'  => 'NotBlank'
        ));

        $this->assertEquals($expected, $validator->getErrors());
    }
}
