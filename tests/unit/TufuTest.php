<?php
/**
 * Project      tufu
 * @author      Giacomo Barbalinardo <info@ready24it.eu>
 * @copyright   2015
 */

use Tufu\Tufu;

class TufuTest extends PHPUnit_Framework_TestCase {

    /**
     * @coversNothing
     */
    public function testHasCheese()
    {
        $tufu = new Tufu();
        $this->assertTrue(!method_exists($tufu, 'hasCheese'));
    }
}
