<?php
/**
 * Project      tufu
 * @author      Giacomo Barbalinardo <info@ready24it.eu>
 * @copyright   2015
 */

$I = new AcceptanceTester($scenario);
$I->amOnPage('/');
$I->see("Welcome to Tufu!");

$I = new AcceptanceTester($scenario);
$I->amOnPage('/documentation');
$I->see("Documentation");
