<?php
/**
 * Project      tufu
 * @author      Giacomo Barbalinardo <info@ready24it.eu>
 * @copyright   2015
 */

use Tufu\Core\RouteManager;

$routesManager = RouteManager::getInstance();

$routesManager->addGetRoute('/', 'App\Controller\Home@welcomeAction');
$routesManager->addGetRoute('/documentation', 'App\Controller\Documentation@indexAction');

