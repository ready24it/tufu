var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');
var uglify = require('gulp-uglify');
var babel = require('rollup-plugin-babel');
var rollup = require('gulp-rollup');

gulp.task('css', function() {
    return gulp.src('app/resources/sass/**/*.scss')
        .pipe(sass())
        .pipe(minifyCSS())
        .pipe(gulp.dest('public/css'))
});

gulp.task('copyfonts', function() {
    gulp.src('node_modules/font-awesome/fonts/**/*.{otf,ttf,woff,woff2,eof,eot,svg}')
        .pipe(gulp.dest('public/fonts'));
});

gulp.task('babelify', function() {
    return gulp.src('app/resources/js/**/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('public/js'));
});

gulp.task('rollup', function () {
    gulp.src(['./app/resources/js/**/*.js'])
    .pipe(rollup({
        entry: './app/resources/js/main.js',
        plugins: [
            babel({
                exclude: 'node_modules/**',
                presets: ['es2015-rollup'],
            }),
        ],
        format: 'iife'
    }))
    .pipe(gulp.dest('./public/js/'));
});

gulp.task('watch', function() {
    //their could be more watchers here ofc
    gulp.watch('app/resources/js/**/*.js', ['rollup']);
    gulp.watch('app/resources/sass/**/*.scss', ['css']);
});


gulp.task('default', ['css', 'rollup']);