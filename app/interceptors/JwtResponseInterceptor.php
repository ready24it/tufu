<?php
/**
 * Project      tufu
 * @author      Giacomo Barbalinardo <info@ready24it.eu>
 * @copyright   2019
 */

namespace App\Interceptor;

use Lindelius\JWT\StandardJWT;
use Tufu\Core\AbstractResponseInterceptor;
use Tufu\Core\ConfigManager;

class JwtResponseInterceptor extends AbstractResponseInterceptor
{

    function beforeResponse(&$response)
    {
        $jwt = new StandardJWT();
        $jwt->exp = time() + (60 * ConfigManager::get('jwt_expires_after')); // Expire after 20 minutes
        $jwt->iat = time();

        $jwt->sub = ['user'=> ['id' => 42, 'name' => 'test-user']];

        $token = $jwt->encode(ConfigManager::get('jwt_pub_key'));
        $response->headers->add(['Authorization'=> 'Bearer ' . $token]);
    }
}
