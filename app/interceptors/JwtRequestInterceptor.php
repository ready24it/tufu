<?php
/**
 * Project      tufu
 * @author      Giacomo Barbalinardo <info@ready24it.eu>
 * @copyright   2019
 */

namespace App\Interceptor;


use Exception;
use Lindelius\JWT\StandardJWT;
use Symfony\Component\HttpFoundation\Request;
use Tufu\Core\AbstractRequestInterceptor;
use Tufu\Core\ConfigManager;

class JwtRequestInterceptor extends AbstractRequestInterceptor
{

    /**
     * @param $request Request
     * @throws Exception
     */
    function beforeRequest(&$request)
    {
        try {
            $auth = $request->headers->get('X-Auth-Token', null);
            if ($auth === null) {
                throw new Exception('Token required.', 500);
            }

            $token = StandardJWT::decode($auth);

            $token->verify(ConfigManager::get('jwt_pri_key'));
        } catch (Exception $e) {
            throw $e;
        }
    }
}
