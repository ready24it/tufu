<?php
/**
 * Project      tufu
 * @author      Giacomo Barbalinardo <info@ready24it.eu>
 * @copyright   2017
 */

namespace App\Controller;

use Tufu\Core\Controller;

class Documentation extends Controller
{
    public function indexAction()
    {
        return $this->render('documentation.twig');
    }
}