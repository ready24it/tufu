<?php
/**
 * Project      tufu
 * @author      Giacomo Barbalinardo <info@ready24it.eu>
 * @copyright   2015
 */

namespace App\Controller;


use App\Model\User;
use Tufu\Core\Controller;

class Home extends Controller{

    public function welcomeAction()
    {
        return $this->render('welcome.twig');
    }


    public function indexAction()
    {
        return $this->render('home.twig', array('foo' => 'bat', 'bar' => 'man'));
    }

    public function listUserAction()
    {
        return $this->render('list.twig', array('items' => User::all()));
    }

    public function addUserAction()
    {
        return $this->render('add_user.twig');
    }

    public function postUser()
    {
        $this->validator->validate($this->request->request->all(), [
            'first_name' => 'NotBlank',
            'last_name'  => 'NotBlank'
        ]);

        if ($this->validator->isValid()) {
            $user = new User();
            $user->first_name = $this->request->get('first_name');
            $user->last_name = $this->request->get('last_name');

            if ($user->save()) {
                return $this->redirect('/user');
            }
        }

        return $this->back();
    }
}